/* ***********************************************************
   *                                                         *
   * Copyright, (C) Honeywell Information Systems Inc., 1982 *
   *                                                         *
   * Copyright (c) 1972 by Massachusetts Institute of        *
   * Technology and Honeywell Information Systems, Inc.      *
   *                                                         *
   *********************************************************** */


ncp_lock_:
          procedure ();

/*             "ncp_lock_" -- this module contains the functions which          */
/*        implement the locking strategies of the NCP.  Notice that these do    */
/*        not include use of the system lock module due to the fact that that   */
/*        module is not willing to include an "unlock all" function.            */

/*        Originally coded by E. W. Meyer.                                      */
/*        Modified by D. M. Wells for conversion to Version II PL/I.            */
/*        Modified by D. M. Wells on 18 June, 1974, to add verify_lock          */
/*             entry point.                                                     */
/*        Modified by D. M. Wells, August 1977, to use new PL/I builtins.       */

/*        Current locking order (from first to last) is:                        */
/*             host_table               -- for host additions, state changes    */
/*             socket_table             -- whenever new sockets are added       */
/*             socket_entry             -- whenever socket attributes changed   */
/*             host entry               -- whenever links are assigned          */
/*             rfc entry                -- (not locked) for resource assignment */
/*                                                                              */
/*        The following dependencies exist:                                     */
/*             socket-activation:       socket-tbl -> socket-entry              */
/*             link-assignment:         socket-entry -> host-entry              */
/*             host-state-change:       host-table -> socket-entry              */
/*                  (incl link unassignment)      -> host-entry                 */

          /* * * * * PARAMETER DECLARATIONS  * * * * * * * */

     declare
         (P_error_code fixed binary (35),                   /* standard Multics status code                   */
          P_lock_ptr pointer)                              /* pointer to the lock in question                */
               parameter;

          /* * * * * AUTOMATIC STORAGE DECLARATIONS  * * * */

     declare
         (processid_code fixed bin (35),
          processid bit (36) aligned,
          tbl_indx fixed binary (17),                       /* index for looping through arrays               */
          wait_times fixed binary (17),                     /* count of calls to pxss$wait this time          */
          start_clock fixed binary (71),                    /* maybe has entry time in it           */
          npr pointer,
          lock_ptr pointer)                                      /* copy of P_lock_ptr internally                 */
               automatic;

          /* * * * * BASED & TEMPLATE REFERENCES * * * * * */

     declare
          based_lock bit (36) aligned
               based;

          /* * * * * EXTERNAL STATIC DECLARATIONS  * * * * */

     declare
         (ncp_params_$syserr_report_error fixed binary (17),
          ncp_params_$wait_event fixed binary (35),
          ncp_tables_$tracing_info bit (36) aligned,
          pds$process_id bit (36) aligned)		
               external static;

     declare
          ncp_tables_$ncp_data
               external static;

     declare
          1 ncp_tables_$lock_tbl external static,           /* the NCP lock table array                       */
             2 lock_count fixed binary (17),                /* count of locks locked now                      */
             2 wait_count fixed binary (17),                /* count of processes waiting on locks            */
             2 lock_array (1 : 10) aligned,
                3 process_id bit (36) aligned,                 /* the process id of a process waiting               */
                3 num_times_slot_used fixed binary (30),    /* number of times this lock slot was used        */
                3 lock_ptr pointer;                         /* pointer to the lock process has locked      */

     declare
         (error_table_$invalid_lock_reset,
          error_table_$lock_not_locked,
          error_table_$locked_by_other_process,
          error_table_$locked_by_this_process,
          error_table_$net_not_up,
          error_table_$net_table_space,
          error_table_$net_timeout)
               fixed binary (35) external static;

          /* * * * * ENTRY & PROCEDURE DECLARATIONS  * * * */

     declare
          ncp_error_ constant entry (fixed bin (35), char (*)),
          ncp_trace_ constant entry options (variable),
          pxss$addevent constant entry (fixed bin (35)),
          pxss$notify constant entry (fixed bin (35)),
          pxss$wait constant entry (),
          syserr constant entry options (variable),
	tc_util$validate_processid entry (bit (36) aligned, fixed bin (35));

     declare
          (addr, clock, hbound, lbound, null, stacq)
               builtin;

          /* * * * * INCLUDE FILES * * * * * * * * * * * * */

     %include ncp_constants_dcls;
     %include ncp_data_dcls;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

                                        /*      Entry point to lock a specified lock (waiting if necessary)   */
                                        /* of the Network Control Program.  Currently, all error conditions   */
                                        /* are handled by calling ncp_error which will crawlout               */

lock_wait:
          entry (P_lock_ptr, P_error_code);

          lock_ptr = P_lock_ptr;
          P_error_code = 0;

          npr = addr (ncp_tables_$ncp_data);

          wait_times = 0;
          start_clock = 0;

          if ncp_tables_$tracing_info ^= ""b
          then call ncp_trace_ (TRACE_NCP_RING, "locking lock at ^p", lock_ptr);

          do while (^ stacq (lock_ptr -> based_lock, pds$process_id, (36)"0"b));
               if start_clock = 0
               then start_clock = clock ();

               if lock_ptr -> based_lock = pds$process_id
               then do;
                    P_error_code = error_table_$locked_by_this_process;
                    call ncp_error_ (P_error_code, "LOCK_attempt_to_relock_same_lock");
                    return;
                    end;

               ncp_tables_$lock_tbl.wait_count = ncp_tables_$lock_tbl.wait_count + 1;
               call pxss$addevent (ncp_params_$wait_event);
               if lock_ptr -> based_lock ^= ""b
               then do;
                    wait_times = wait_times + 1;
                    if (wait_times >= 100) | (clock () > start_clock + 300000000)
                    then do;                                          /* something must be wrong, give up               */
                         ncp_tables_$lock_tbl.wait_count = ncp_tables_$lock_tbl.wait_count - 1;
                         P_error_code = error_table_$net_timeout;
                         call ncp_error_ (P_error_code, "LOCK_waited_too_long_for_lock");
                         return;
                         end;

                    call pxss$wait ();
                    end;
               ncp_tables_$lock_tbl.wait_count = ncp_tables_$lock_tbl.wait_count - 1;

               if npr -> ncp_dt.ncp_up <= NCP_DOWN
               then do;
                    P_error_code = error_table_$net_not_up;
                    return;
                    end;
               end;

                                                            /* the lock is now locked                         */
          do tbl_indx = lbound (ncp_tables_$lock_tbl.lock_array, 1) by 1 to hbound (ncp_tables_$lock_tbl.lock_array, 1)
                    while (^ stacq (ncp_tables_$lock_tbl.lock_array (tbl_indx).process_id, pds$process_id, (36)"0"b));
               end;
          if tbl_indx > hbound (ncp_tables_$lock_tbl.lock_array, 1)
          then do;                                          /* we couldn't find an empty lock slot entry      */
               call unlock (lbound (ncp_tables_$lock_tbl.lock_array, 1) - 1, lock_ptr);
               P_error_code = error_table_$net_table_space;
               call ncp_error_ (P_error_code, "LOCK_not_free_memory_slots");
               return;
               end;

          ncp_tables_$lock_tbl.lock_array (tbl_indx).lock_ptr = lock_ptr;
          ncp_tables_$lock_tbl.lock_array (tbl_indx).num_times_slot_used
                    = ncp_tables_$lock_tbl.lock_array (tbl_indx).num_times_slot_used + 1;
          ncp_tables_$lock_tbl.lock_count = ncp_tables_$lock_tbl.lock_count + 1;

          return;

          /* * * * * * * * * * * * * * * * * * * * * * * * */

                                        /*      Entry point to unlock a specified lock (which must be         */
                                        /* locked).  Error conditions are handled by calling ncp_error        */
                                        /* which will crawlout.                                               */

lock_unlock:
          entry (P_lock_ptr, P_error_code);

          P_error_code = 0;

          lock_ptr = P_lock_ptr;

          do tbl_indx = lbound (ncp_tables_$lock_tbl.lock_array, 1) by 1 to hbound (ncp_tables_$lock_tbl.lock_array, 1)
                    while ((pds$process_id ^= ncp_tables_$lock_tbl.lock_array (tbl_indx).process_id)
                    | (lock_ptr ^= ncp_tables_$lock_tbl.lock_array (tbl_indx).lock_ptr));
               end;
          if tbl_indx > hbound (ncp_tables_$lock_tbl.lock_array, 1)
          then do;                                          /* we failed to find the lock, -- error           */
               P_error_code = error_table_$lock_not_locked;
               call ncp_error_ (P_error_code, "LOCK_lock_already_unlocked");
               return;
               end;

          call unlock (tbl_indx, lock_ptr);

          if ncp_tables_$lock_tbl.wait_count ^= 0
          then call pxss$notify (ncp_params_$wait_event);

          return;

          /* * * * * * * * * * * * * * * * * * * * * * * * */

                                        /*      Entry point to unlock any (zero or more) NCP locks which      */
                                        /* have been locked by this mechanism.                                */

unlock_all:
          entry (P_error_code);

          P_error_code = 0;
          lock_ptr = null ();                               /* check for any programming errors               */

          do tbl_indx = lbound (ncp_tables_$lock_tbl.lock_array, 1) by 1 to hbound (ncp_tables_$lock_tbl.lock_array, 1);
                                                            /* loop through the entire table                  */
               if pds$process_id = ncp_tables_$lock_tbl.lock_array (tbl_indx).process_id
               then do;
                    lock_ptr = ncp_tables_$lock_tbl.lock_array (tbl_indx).lock_ptr;      /* make pointer for nulling   */

                    call unlock (tbl_indx, lock_ptr);
                    end;
               end;

          if ncp_tables_$lock_tbl.wait_count ^= 0
          then call pxss$notify (ncp_params_$wait_event);

          return;

          /* * * * * * * * * * * * * * * * * * * * * * * * */

verify_lock:
          entry (P_lock_ptr, P_error_code);

          P_error_code = 0;

verify_lock_retry:
	processid = P_lock_ptr -> based_lock;
	if processid = ""b
	     then do;
	     P_error_code = error_table_$lock_not_locked;
	     return;
	end;
	if processid = pds$process_id then do;
	     P_error_code = error_table_$locked_by_this_process;
	     return;
	end;

	call tc_util$validate_processid (processid, processid_code);
	if processid_code = 0 then do;
	     P_error_code = error_table_$locked_by_other_process;
	     return;
	end;

	if stacq (P_lock_ptr -> based_lock, pds$process_id, processid) then do;
	     P_error_code = error_table_$invalid_lock_reset;
	     return;
	end;
	else goto verify_lock_retry;


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

unlock:
          procedure (p_tbl_indx, p_lock_ptr);

          /* * * * * PARAMETER DECLARATIONS  * * * * * * * */

     declare
         (p_tbl_indx fixed binary (17),
          p_lock_ptr pointer)
               parameter;

          /* * * * * AUTOMATIC STORAGE DECLARATIONS  * * * */

     declare
          slot_id_ptr pointer
               automatic;

          /* * * * * * * * * * * * * * * * * * * * * * * * */

          if ncp_tables_$tracing_info ^= ""b
          then call ncp_trace_ (TRACE_NCP_RING, "unlocking lock at ^p", p_lock_ptr);

          if p_tbl_indx >= 0
          then do;
               ncp_tables_$lock_tbl.lock_array (p_tbl_indx).lock_ptr = null ();

               slot_id_ptr = addr (ncp_tables_$lock_tbl.lock_array (p_tbl_indx).process_id);
               if ^ stacq (slot_id_ptr -> based_lock, (36)"0"b, pds$process_id)
               then call syserr (ncp_params_$syserr_report_error,
                              "NCP:  lock array removal failed at ^p (on lock at ^p)", slot_id_ptr, p_lock_ptr);
               end;

          ncp_tables_$lock_tbl.lock_count = ncp_tables_$lock_tbl.lock_count - 1;

          if ^ stacq (p_lock_ptr -> based_lock, (36)"0"b, pds$process_id)
          then call syserr (ncp_params_$syserr_report_error,
                         "NCP:  unlock failed at ^p", p_lock_ptr);

          return;

end;      /* end unlock                                    */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* BEGIN MESSAGE DOCUMENTATION

   Message:
   NCP:  lock array removal failed at LOC (on lock at LOC)

   S: $log

   T: $run

   M: $err
   The Network Control Program attempts to recover and keep running.

   A: $note
   If this problem persists, it might indicate that the stacq
   instruction is failing.

   Message:
   NCP:  unlock failed at LOC

   S: $log

   T: $run

   M: $err
   The Network Control Program attempts to recover and continue running.

   A: $note
   If this problem persists, if might indicate that the stacq
   instruction is failing.


   END MESSAGE DOCUMENTATION */

end;      /* end ncp_lock_                                 */

/* ***********************************************************
   *                                                         *
   * Copyright, (C) Honeywell Information Systems Inc., 1984 *
   *                                                         *
   *********************************************************** */
net_signal_handler_:
          procedure () options (support);

/*             "net_signal_handler_" -- manager for the special IPS processing  */
/*        that the Network typewriter interface must do in order to support the */
/*        Network Virtual Terminal.                                             */


/*        Created by D. M. Wells on 17 December, 1973.                          */
/*        Last modified by D. M. Wells on 2 April, 1974 to allow interrupts to  */
/*                  be identified on a per NCP pin basis (thus allowing IOSIMS  */
/*                  to identify QUITs on a per stream basis).                   */
/*        Last modified by D. M. Wells, August, 1975, to prepare for change     */
/*                  to the "neti" signal (instead of "quit")                    */
/*        Last modified by D. M. Wells, March, 1976, to use the SCT mechanism   */
/*        Last modified by D. M. Wells, October, 1976, to remove pre-MR4.0      */
/*             compatability code -- notably patching of stack signal ptr.      */

          /* * * * * PARAMETER DECLARATIONS  * * * * * * * */

     declare
         (P_ncp_indx bit (36),
          P_error_code fixed binary (35),
          P_continue_switch bit (1) aligned,
          P_condition_name character (*),
          P_data_ptr pointer,
          (P_crawlout_ptr, P_info_ptr, P_mc_ptr) pointer,
          P_signal_handler entry (char (*), ptr, ptr, ptr, bit (1) aligned))
               parameter;

          /* * * * * AUTOMATIC STORAGE DECLARATIONS  * * * */

     declare
         (indx fixed binary (12),
          arg_ncp_indx bit (36) aligned,
          previous_ips_mask bit (36) aligned,
          arg_data_ptr pointer,
          arg_signal_handler variable entry (ptr, char (*), ptr, ptr, bit (1) aligned))
               automatic;

          /* * * * * INTERNAL STATIC DECLARATIONS  * * * * */

     declare
         (count_of_handler_data_slots_used fixed binary (12) initial (0),
          signal_notify_channel fixed binary (71) initial (0))
               internal static;

     declare
          1 handler_data (1 : 40) aligned internal static,
             2 data_ptr pointer,
             2 ncp_indx bit (36) aligned initial ((40)(36)"0"b),
             2 signal_count fixed binary (30),
             2 signal_procedure entry (char (*), ptr, ptr, ptr, bit (1) aligned);

          /* * * * * TEXT SECTION REFERENCES * * * * * * * */

     declare
         (EMPTY_ENTRY         initial ((36)"0"b),
          CHANGING_ENTRY      initial ((36)"1"b))
               bit (36) aligned internal static options (constant);

     declare
          MASK_EVERYTHING initial ((36)"0"b)
               bit (36) aligned internal static options (constant);


          /* * * * * EXTERNAL STATIC DECLARATIONS  * * * * */

     declare
         (error_table_$bad_index,
          error_table_$noalloc,
          error_table_$noentry)
               fixed binary (35) external static;

          /* * * * * ENTRY & PROCEDURE DECLARATIONS  * * * */

     declare
          hcs_$set_ips_mask constant entry (bit (36) aligned, bit (36) aligned),
          ipc_$create_ev_chn constant entry (fixed bin (71), fixed bin (35)),
          ipc_$delete_ev_chn constant entry (fixed bin (71), fixed bin (35)),
          ipc_$read_ev_chn constant entry (fixed bin (71), fixed bin (1), ptr, fixed bin (35)),
          ncp_$disable_interrupts constant entry (bit (36), fixed bin (35)),
          ncp_$enable_interrupts constant entry (bit (36), fixed bin (71), fixed bin (35)),
          sct_manager_$set constant entry (fixed bin (17), entry (ptr, char (*), ptr, ptr, bit (1) aligned), fixed bin (35)),
          stacq constant entry (ptr, bit (36) aligned, bit (36) aligned) returns (bit (1) aligned);

     declare
         (addr, hbound, lbound, stac, substr)
               builtin;

          /* * * * * STACK REFERENCES  * * * * * * * * * * */

     declare
          net_stacq_error_
               condition;

          /* * * * * INCLUDE FILES * * * * * * * * * * * * */

%include net_event_template;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

setup_signal_handler:
          entry (P_ncp_indx, P_signal_handler, P_data_ptr, P_error_code);

          P_error_code = 0;

          arg_ncp_indx = P_ncp_indx;
          arg_signal_handler = P_signal_handler;
          arg_data_ptr = P_data_ptr;

          if (arg_ncp_indx = EMPTY_ENTRY) | (arg_ncp_indx = CHANGING_ENTRY)
          then do;
               P_error_code = error_table_$bad_index;
               return;
               end;

          call hcs_$set_ips_mask (MASK_EVERYTHING, previous_ips_mask);

          do indx = lbound (handler_data, 1) by 1 to hbound (handler_data, 1);
               if stacq (addr (handler_data (indx).ncp_indx), arg_ncp_indx, CHANGING_ENTRY)
               then do;
                    handler_data (indx).signal_procedure = arg_signal_handler;
                    handler_data (indx).data_ptr = arg_data_ptr;

		call allow_network_signals (arg_ncp_indx, (0));

                    if ^ stacq (addr (handler_data (indx).ncp_indx), CHANGING_ENTRY, arg_ncp_indx)
                    then signal net_stacq_error_;

                    call hcs_$set_ips_mask (previous_ips_mask, (""b));
                    return;
                    end;
               end;

          do indx = lbound (handler_data, 1) by 1 to hbound (handler_data, 1)
                    while (^ stac (addr (handler_data (indx).ncp_indx), CHANGING_ENTRY));
               end;

          if indx > hbound (handler_data, 1)
          then do;
               call hcs_$set_ips_mask (previous_ips_mask, (""b));

               P_error_code = error_table_$noalloc;
               return;
               end;

          handler_data (indx).data_ptr = arg_data_ptr;
          handler_data (indx).signal_procedure = arg_signal_handler;
          handler_data (indx).signal_count = 0;             /* reset the signal count for this socket to zero */


          if ^ stacq (addr (handler_data (indx).ncp_indx), CHANGING_ENTRY, arg_ncp_indx)
          then signal net_stacq_error_;

          count_of_handler_data_slots_used = count_of_handler_data_slots_used + 1;

          call allow_network_signals (arg_ncp_indx, (0));

          call hcs_$set_ips_mask (previous_ips_mask, (""b));

          return;

          /* * * * * * * * * * * * * * * * * * * * * * * * */

remove_signal_handler:
          entry (P_ncp_indx, P_error_code);

          P_error_code = 0;

          arg_ncp_indx = P_ncp_indx;

          if (arg_ncp_indx = EMPTY_ENTRY) | (arg_ncp_indx = CHANGING_ENTRY)
          then do;
               P_error_code = error_table_$bad_index;
               return;
               end;

          call hcs_$set_ips_mask (MASK_EVERYTHING, previous_ips_mask);

          do indx = lbound (handler_data, 1) by 1 to hbound (handler_data, 1)
                    while (^ stacq (addr (handler_data (indx).ncp_indx), arg_ncp_indx, CHANGING_ENTRY));
               end;

          if indx > hbound (handler_data, 1)
          then do;
               call hcs_$set_ips_mask (previous_ips_mask, (""b));

               P_error_code = error_table_$noentry;
               return;
               end;

          if ^ stacq (addr (handler_data (indx).ncp_indx), CHANGING_ENTRY, EMPTY_ENTRY)
          then signal net_stacq_error_;

          count_of_handler_data_slots_used = count_of_handler_data_slots_used - 1;

          call disallow_network_signals (arg_ncp_indx, (0));

          call hcs_$set_ips_mask (previous_ips_mask, (""b));

          return;

          /* * * * * * * * * * * * * * * * * * * * * * * * */

neti_signal_handler:
          entry (P_mc_ptr, P_condition_name, P_info_ptr, P_crawlout_ptr, P_continue_switch);

          do while (network_signal_received (arg_ncp_indx, (0)));
               do indx = lbound (handler_data, 1) by 1 to hbound (handler_data, 1)
                         while (handler_data (indx).ncp_indx ^= arg_ncp_indx);
                    end;

               if indx <= hbound (handler_data, 1)
               then do;
                    arg_signal_handler = handler_data (indx).signal_procedure;
                    call arg_signal_handler (P_mc_ptr, P_condition_name, (handler_data (indx).data_ptr), P_crawlout_ptr, ("0"b));
                    end;
               end;

          return;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

allow_network_signals:
          procedure (p_ncp_indx_of_pin, p_err_code);

          /* * * * * PARAMETER DECLARATIONS  * * * * * * * */

     declare
         (p_err_code fixed binary (35),
          p_ncp_indx_of_pin bit (36) aligned)
               parameter;

          /* * * * * AUTOMATIC STORAGE DECLARATIONS * * * * */

     declare
          valid_event fixed binary (1)
               automatic;

     declare
          1 event_message aligned automatic like event_message_template;

          /* * * * * * * * * * * * * * * * * * * * * * * * */

          if signal_notify_channel = 0
          then do;
               call setup_environment (p_err_code);
               if p_err_code ^= 0
               then return;
               end;

          call ncp_$enable_interrupts ((p_ncp_indx_of_pin), signal_notify_channel, p_err_code);

          return;

          /* * * * * * * * * * * * * * * * * * * * * * * * */

disallow_network_signals:
          entry (p_ncp_indx_of_pin, p_err_code);

          call ncp_$disable_interrupts ((p_ncp_indx_of_pin), (0));

          if count_of_handler_data_slots_used = 0
          then call remove_environment ((0));

          p_err_code = 0;

          return;

          /* * * * * * * * * * * * * * * * * * * * * * * * */

network_signal_received:
          entry (p_ncp_indx_of_pin, p_err_code) returns (bit (1) aligned);

          call ipc_$read_ev_chn (signal_notify_channel, valid_event, addr (event_message), p_err_code);
          if p_err_code ^= 0
          then do;
               signal_notify_channel = 0;
               return ("0"b);
               end;

          if valid_event = 0
          then return ("0"b);

          p_ncp_indx_of_pin = substr (event_message.message, 37, 36);

          return ("1"b);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

setup_environment:
          procedure (p_err_code);

          /* * * * * PARAMETER DECLARATIONS  * * * * * * * */

     declare
          p_err_code fixed binary (35)
               parameter;

          /* * * * * INCLUDE FILES * * * * * * * * * * * * */
      307
%include static_handlers;

          /* * * * * * * * * * * * * * * * * * * * * * * * */

          call ipc_$create_ev_chn (signal_notify_channel, p_err_code);
          if p_err_code ^= 0
          then return;

          call sct_manager_$set (neti_sct_index, neti_signal_handler, p_err_code);

          return;

          /* * * * * * * * * * * * * * * * * * * * * * * * */

remove_environment:
          entry (p_err_code);

          p_err_code = 0;

          call ipc_$delete_ev_chn (signal_notify_channel, (0));

          signal_notify_channel = 0;

          return;

end;      /* end setup_environment                         */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

end;      /* end allow_network_signals                     */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

          /* end net_signal_handler_                       */
end;
